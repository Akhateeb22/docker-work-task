FROM centos:latest	

RUN mkdir -p /data/db/ && \
echo -e "\
[mongodb]\n\
name=MongoDB Repository\n\
baseurl=https://repo.mongodb.org/yum/redhat/7Server/mongodb-org/3.0/x86_64/\n\
gpgcheck=0\n\
enabled=1\n" >> /etc/yum.repos.d/mongodb.repo && \
yum update -y && \
yum install -y mongodb-org


CMD mongod

EXPOSE 27017
